import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:student_api_test/providers/auth_provider.dart';
import 'package:student_api_test/screens/home_screen.dart';
import 'package:student_api_test/screens/login_screen.dart';
import 'file:///C:/flutter_projects/student_api_test/lib/widget/student_model.dart';
import 'package:student_api_test/screens/signin_screen.dart';
import 'package:student_api_test/screens/student_details.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AuthProvider>.value(
            value: AuthProvider(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          initialRoute: '/',
          routes: {
            '/': (context) => LoginScreen(),
            '/signup': (context) => SignupScreen(),
            '/home': (context) => HomeScreen(),

          },
        ));
  }
}