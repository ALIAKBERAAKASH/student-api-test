import 'dart:io';

import 'package:student_api_test/data/delete_student_response.dart';
import 'package:student_api_test/data/signup_credentials.dart';
import 'package:student_api_test/data/signup_response.dart';
import 'package:student_api_test/data/get_all_students_response.dart';
import 'package:student_api_test/data/insert_student_response.dart';
import 'package:student_api_test/data/login_credentials.dart';
import 'package:student_api_test/data/login_response.dart';
import 'package:http/http.dart' as http;
import 'package:student_api_test/data/student.dart';
import 'package:student_api_test/data/update_student_response.dart';

class Repository {

  String url = 'http://weavertechbd.com/student/api';

  //Create new user
  Future<SignupResponse> signUp(SignUpCredentials signUpCredentials) async{
    final response = await http.post('$url/signup',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
      },
      body: signUpCredentialsToJson(signUpCredentials),
    );
    return signupResponseFromJson(response.body);
  }

  //Login user with username and password
  Future<LoginResponse> login(LoginCredentials loginCredentials) async{
    final response = await http.post('$url/login',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: loginCredentialsToJson(loginCredentials),
    );
    return loginResponseFromJson(response.body);
  }

  //Fetch all students data
  Future<List<GetAllStudentsResponse>> getAllStudents(String token) async{
    final response = await http.get('$url/student',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: '$token',
      },
    );
    
    return getAllStudentsResponseFromJson(response.body);
  }

  //Insert a student
  Future<InsertStudentResponse> insertStudent(Student student, String token) async{
    final response = await http.post('$url/student',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: '$token',
      },
      body: studentToJson(student),
    );
    return insertStudentResponseFromJson(response.body);
  }

  //Update a student
  Future<UpdateStudentResponse> updateStudent(Student student, String token) async{
    final response = await http.put('$url/student/${student.id}',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: '$token',
      },
      body: studentToJson(student),
    );
    return updateStudentResponseFromJson(response.body);
  }


  //Delete a student
  Future<DeleteStudentResponse> deleteStudentById(int id, String token) async{
    final response = await http.delete('$url/student/$id',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: '$token',
      },
    );
    return deleteStudentResponseFromJson(response.body);
  }

  //getStudentById
  Future<Student> getStudentById(int id, String token) async{
    final response = await http.delete('$url/student/$id',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: '$token',
      },
    );
    return studentFromJson(response.body);
  }

}