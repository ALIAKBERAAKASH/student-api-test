import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:student_api_test/data/get_all_students_response.dart';
import 'package:student_api_test/providers/auth_provider.dart';
import 'package:student_api_test/repository/repository.dart';
import 'package:student_api_test/screens/custom_dialog.dart';
import 'package:student_api_test/widget/student_model.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {

    final AuthProvider authProvider = Provider.of<AuthProvider>(context);
    final Repository repository = new Repository();

    List<GetAllStudentsResponse> myList = authProvider.studentList;

    repository.getAllStudents(authProvider.token).then((value) {

    authProvider.setStudentList(value);
    print(value.toString());

    });



    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: ListView.builder(
            itemCount: myList.length,
            itemBuilder: (context, index){
              return StudentModel(myList[index]);
          },),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        elevation: 4.0,
        icon: const Icon(Icons.add),
        label: const Text('Add a task'),
        onPressed: () {

          showDialog(
              context: context,
              builder: (BuildContext context){
                return CustomAlertDialog();
              }
          );

        },
      ),
      floatingActionButtonLocation:
      FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            )
          ],
        ),
      ),
    );
  }
}
