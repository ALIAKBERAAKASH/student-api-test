import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:student_api_test/data/login_credentials.dart';
import 'package:student_api_test/providers/auth_provider.dart';
import 'package:student_api_test/repository/repository.dart';
import 'package:student_api_test/screens/home_screen.dart';
import 'package:student_api_test/widget/click_signup.dart';

class LoginScreen extends StatelessWidget {
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var hp =MediaQuery.of(context).size.height;
    var hw =MediaQuery.of(context).size.width;

    Repository repository = new Repository();
    final AuthProvider authProvider = Provider.of<AuthProvider>(context);

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
             Text('LogIn',style: TextStyle(color: Colors.teal,fontSize: 30),),
             SizedBox(height: hp*0.15,),
            Padding(
            padding: EdgeInsets.all(15),
            child: TextField(
              obscureText: false,
              controller:  emailController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Email",
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: TextField(
              obscureText: true,
              controller:  passwordController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Password",
              ),
            ),
          ),

             SizedBox(height: hp*0.03),
             Row(
               mainAxisAlignment: MainAxisAlignment.end,
               children: <Widget>[
                 Padding(
                   padding: const EdgeInsets.only(right: 20),
                   child: Text("Forgate Password ?",style:
                   TextStyle(decoration:TextDecoration.underline,color:Colors.indigo ,),textAlign: TextAlign.right,
                   ),
                 ),
               ],
             ),
             SizedBox(height: hp*0.01,),
          SizedBox(
            width: hw*0.70,
            height: hp*0.05,
            child: RaisedButton(
              child: Text('LOGIN',style: TextStyle(color:Colors.white,
                  fontSize: 15,
                  fontStyle: FontStyle.normal,
                  fontWeight:FontWeight.w700),),
              color: Colors.teal,
              onPressed: (){

                String email = emailController.text;
                String password = passwordController.text;

                print("email : $email \n password : $password");

                repository.login(LoginCredentials(
                  email: email,
                  password: password
                )).then((value) {
                  if(value.success)
                    {
                      print('Login Success');
                      authProvider.setToken(value.token);
                      //print(value.token);
                      Navigator.pushReplacementNamed(context, '/home');
                    }
                  else
                    print('Login Failed');
                });



              },
              shape: StadiumBorder(),
            ),
          ),
             SizedBox(height: hp*0.01),
             ClickSignUp(),
        ],
      ),
    );
  }
}
