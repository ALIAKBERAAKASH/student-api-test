import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:student_api_test/data/get_all_students_response.dart';
import 'package:student_api_test/data/student.dart';
import 'package:student_api_test/providers/auth_provider.dart';
import 'package:student_api_test/repository/repository.dart';

class CustomAlertDialog extends StatelessWidget {

  TextEditingController nameCtrl=TextEditingController();
  TextEditingController emailCtrl=TextEditingController();
  TextEditingController phoneCtrl=TextEditingController();
  TextEditingController faNameCtrl=TextEditingController();
  bool marriedCheck=true;
  bool maleRadio=false;
  bool femaleRadio=false;
  String bloodGroup = "O+";

 /*void _chageRadioButton(){
   if(maleRadio==true)


 }*/

  @override
  Widget build(BuildContext context) {

    AuthProvider provider = Provider.of<AuthProvider>(context);
    Repository repository = new Repository();

    var hp =MediaQuery.of(context).size.height;
    var hw =MediaQuery.of(context).size.width;


    return SingleChildScrollView(
      child: Dialog(
          shape:RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0)),
            height: hp*0.90,
            width: hw*0.95,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      obscureText:false,
                      controller: nameCtrl ,
                      decoration: InputDecoration(hintText: 'Name'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      obscureText:false,
                      controller: emailCtrl ,
                      decoration: InputDecoration(hintText: 'Email'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      obscureText:false,
                      controller: phoneCtrl ,
                      decoration: InputDecoration(hintText: 'phone'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      obscureText:false,
                      controller: faNameCtrl ,
                      decoration: InputDecoration(hintText: 'Father Name'),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text('Gender:'),
                      Radio(
                        groupValue: maleRadio,
                       // onChanged: _chageRadioButton,
                      ),
                      Text('Male'),
                      Radio(
                        groupValue: femaleRadio,
                       // onChanged:_chageRadioButton ,
                      ),
                      Text('Female'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('Marital Status :'),
                      Checkbox(value: marriedCheck,),
                      Text('Marreid'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('Blood Group:'),
                      SizedBox(width: 40,),
                      DropdownButton<String>(
                        items: <String>['AB+', 'AB-', 'A+', 'A-','B+','B-','O+','O-'].map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        onChanged: (item) {

                          bloodGroup = item;

                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 40,),
                  RaisedButton(child: Text("Submit"),onPressed: (){

                    Student student = Student (
                      id: -1,
                      name : nameCtrl.text,
                      email: emailCtrl.text,
                      contactNo: phoneCtrl.text,
                      address: "Antopolis",
                      fathersName: faNameCtrl.text,
                      gender: maleRadio ? 1 : 2,
                      maritialStatus: marriedCheck,
                      dob: DateTime.now(),
                      bloodGroup: bloodGroup,
                    );

                    repository.insertStudent(student, provider.token).then((value) {

                      if(value.success)
                        {

                          Student student = value.student;

                          GetAllStudentsResponse myResponse = GetAllStudentsResponse(
                            id: student.id,
                            name : student.name,
                            email: student.email,
                            contactNo: student.contactNo,
                            address: student.address,
                            fathersName: student.fathersName,
                            gender: student.gender == 1 ? "Male" : "Female",
                            maritialStatus: student.maritialStatus ? "Married" : "Unmarried",
                            dob: DateTime.now(),
                            bloodGroup: bloodGroup,
                            createdAt: DateTime.now(),
                            updatedAt: DateTime.now(),
                          );

                          provider.addToStudentList(myResponse);
                          Navigator.pop(context);
                        }

                    });

                  },color:Color(0xFF17ead9)),
                ],
              ),
            ),
          ),
      ),
    );
  }
}
