import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:student_api_test/data/student.dart';
import 'package:student_api_test/providers/auth_provider.dart';
import 'package:student_api_test/repository/repository.dart';

class StudentDetails extends StatefulWidget {

  final int id;
  const StudentDetails(this.id);

  @override
  _StudentDetailsState createState() => _StudentDetailsState();
}

class _StudentDetailsState extends State<StudentDetails> {
  @override
  Widget build(BuildContext context) {

    AuthProvider provider = Provider.of<AuthProvider>(context);
    Repository repository = new Repository();

    Student student = Student(
      name: "name",
      email: "email",
      contactNo: "12345678",
      fathersName: "father's name",
      gender: 1,
      maritialStatus: false,
      bloodGroup: "blood group"
    );

    repository.getStudentById(widget.id, provider.token).then((value)
    {
      setState(() {
        student = value;
      });

    });

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Name : "),
               Text(student.name),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Email : "),
               Text(student.email),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Phone : "),
               Text(student.contactNo),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Father's name : "),
               Text(student.fathersName),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Gender : "),
                Text(student.gender == 1 ? "Male" : "Female"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Marital Status : "),
               Text(student.maritialStatus ? "Married" : "Unmarried"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Blood Group : "),
                Text(student.bloodGroup),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
