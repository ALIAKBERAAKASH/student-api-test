import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:student_api_test/data/signup_credentials.dart';
import 'package:student_api_test/providers/auth_provider.dart';
import 'package:student_api_test/repository/repository.dart';
import 'package:student_api_test/widget/singin_button.dart';


class SignupScreen extends StatelessWidget {

  TextEditingController nameController=TextEditingController();
  TextEditingController emailController=TextEditingController();
  TextEditingController firstPassController=TextEditingController();
  TextEditingController secondPassController=TextEditingController();

  @override
  Widget build(BuildContext context) {

    Repository repository = new Repository();
    final AuthProvider authProvider = Provider.of<AuthProvider>(context);

    var hp =MediaQuery.of(context).size.height;
    var hw =MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.white70,),
       body: Column(
         mainAxisAlignment: MainAxisAlignment.center,
         children: <Widget>[
           Text('Signup',style: TextStyle(color: Colors.teal,fontSize: 30),),
           SizedBox(height: hp*0.10,),
           Padding(
             padding: EdgeInsets.all(15),
             child: TextField(
               obscureText: false,
               controller:  nameController,
               decoration: InputDecoration(
                 border: OutlineInputBorder(),
                 labelText: "Name",
               ),
             ),
           ),
           Padding(
             padding: EdgeInsets.all(15),
             child: TextField(
               obscureText: false,
               controller:  emailController,
               decoration: InputDecoration(
                 border: OutlineInputBorder(),
                 labelText: "Email",
               ),
             ),
           ),
           Padding(
             padding: EdgeInsets.all(15),
             child: TextField(
               obscureText: true,
               controller:  firstPassController,
               decoration: InputDecoration(
                 border: OutlineInputBorder(),
                 labelText: "Password",
               ),
             ),
           ),
           Padding(
             padding: EdgeInsets.all(15),
             child: TextField(
               obscureText: true,
               controller:  secondPassController,
               decoration: InputDecoration(
                 border: OutlineInputBorder(),
                 labelText: "Confirm Password",
               ),
             ),
           ),
           SizedBox(height: hp*0.03,),
           SizedBox(
             width: hw*0.70,
             height: hp*0.05,
             child: RaisedButton(
               child: Text('SIGNUP',style: TextStyle(color:Colors.white,
                   fontSize: 15,
                   fontStyle: FontStyle.normal,
                   fontWeight:FontWeight.w700),),
               color: Colors.teal,
               onPressed: (){

                 String name = nameController.text;
                 String email = emailController.text;
                 String password = firstPassController.text;

                 print("email : $email \n password : $password");

                 repository.signUp(SignUpCredentials(
                     name: name,
                     email: email,
                     password: password
                 )).then((value) {
                   if(value.success)
                   {
                     print('SignUp Success');
                     authProvider.setToken(value.token);
                     //print(value.token);
                     Navigator.pushReplacementNamed(context, '/home');
                   }
                   else
                     print('Signup Failed');
                 });


               },
               shape: StadiumBorder(),
             ),
           ),
         ],
       ),
    );
  }
}
