import 'package:flutter/cupertino.dart';
import 'package:student_api_test/data/get_all_students_response.dart';

class AuthProvider extends ChangeNotifier {

  String _token="";
  bool _isLoggedIn = false;
  List<GetAllStudentsResponse> _studentList = List<GetAllStudentsResponse>();

  get token => _token;
  get isLoggedI => _isLoggedIn;
  get studentList => _studentList;

  void setToken(String str)
  {
    _token = "Bearer "+str;
    notifyListeners();
  }

  void addToStudentList(GetAllStudentsResponse response)
  {
    _studentList.add(response);
    notifyListeners();
  }

  void setStudentList(List<GetAllStudentsResponse> response)
  {


    _studentList = response;
    notifyListeners();
  }

  set isLoggedIn(bool value)
  {
    _isLoggedIn = value;
    notifyListeners();
  }



}