import 'dart:convert';

UpdateStudentResponse updateStudentResponseFromJson(String str) => UpdateStudentResponse.fromJson(json.decode(str));

String updateStudentResponseToJson(UpdateStudentResponse data) => json.encode(data.toJson());

class UpdateStudentResponse {
  bool success;

  UpdateStudentResponse({
    this.success,
  });

  factory UpdateStudentResponse.fromJson(Map<String, dynamic> json) => UpdateStudentResponse(
    success: json["success"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
  };
}