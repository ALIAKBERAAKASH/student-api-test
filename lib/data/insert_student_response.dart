import 'dart:convert';

import 'package:student_api_test/data/student.dart';



InsertStudentResponse insertStudentResponseFromJson(String str) => InsertStudentResponse.fromJson(json.decode(str));

String insertStudentResponseToJson(InsertStudentResponse data) => json.encode(data.toJson());

class InsertStudentResponse {
  bool success;
  Student student;

  InsertStudentResponse({
    this.success,
    this.student,
  });

  factory InsertStudentResponse.fromJson(Map<String, dynamic> json) => InsertStudentResponse(
    success: json["success"],
    student: Student.fromJson(json["student"]),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "student": student.toJson(),
  };
}

