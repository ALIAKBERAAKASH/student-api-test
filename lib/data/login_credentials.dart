import 'dart:convert';

LoginCredentials loginCredentialsFromJson(String str) => LoginCredentials.fromJson(json.decode(str));

String loginCredentialsToJson(LoginCredentials data) => json.encode(data.toJson());

class LoginCredentials {
  String email;
  String password;

  LoginCredentials({
    this.email,
    this.password,
  });

  factory LoginCredentials.fromJson(Map<String, dynamic> json) => LoginCredentials(
    email: json["email"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": password,
  };
}