import 'dart:convert';

DeleteStudentResponse deleteStudentResponseFromJson(String str) => DeleteStudentResponse.fromJson(json.decode(str));

String deleteStudentResponseToJson(DeleteStudentResponse data) => json.encode(data.toJson());

class DeleteStudentResponse {
  bool success;

  DeleteStudentResponse({
    this.success,
  });

  factory DeleteStudentResponse.fromJson(Map<String, dynamic> json) => DeleteStudentResponse(
    success: json["success"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
  };
}