import 'dart:convert';

Student studentFromJson(String str) => Student.fromJson(json.decode(str));

String studentToJson(Student data) => json.encode(data.toJson());

class Student {
  int id;
  String name;
  String email;
  String contactNo;
  String address;
  String fathersName;
  int gender;
  bool maritialStatus;
  DateTime dob;
  String bloodGroup;

  Student({
    this.id,
    this.name,
    this.email,
    this.contactNo,
    this.address,
    this.fathersName,
    this.gender,
    this.maritialStatus,
    this.dob,
    this.bloodGroup,
  });

  factory Student.fromJson(Map<String, dynamic> json) => Student(
    id: json['id'],
    name: json["name"],
    email: json["email"],
    contactNo: json["contact_no"],
    address: json["address"],
    fathersName: json["fathers_name"],
    gender: json["gender"],
    maritialStatus: json["maritial_status"],
    dob: DateTime.parse(json["dob"]),
    bloodGroup: json["blood_group"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "contact_no": contactNo,
    "address": address,
    "fathers_name": fathersName,
    "gender": gender,
    "maritial_status": maritialStatus,
    "dob": "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
    "blood_group": bloodGroup,
  };
}
