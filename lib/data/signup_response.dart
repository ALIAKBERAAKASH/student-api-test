import 'dart:convert';

SignupResponse signupResponseFromJson(String str) => SignupResponse.fromJson(json.decode(str));

String signupResponseToJson(SignupResponse data) => json.encode(data.toJson());

class SignupResponse {
  bool success;
  String token;

  SignupResponse({
    this.success,
    this.token,
  });

  factory SignupResponse.fromJson(Map<String, dynamic> json) => SignupResponse(
    success: json["success"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "token": token,
  };
}