import 'dart:convert';

List<GetAllStudentsResponse> getAllStudentsResponseFromJson(String str) => List<GetAllStudentsResponse>.from(json.decode(str).map((x) => GetAllStudentsResponse.fromJson(x)));

String getAllStudentsResponseToJson(List<GetAllStudentsResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetAllStudentsResponse {
  int id;
  String name;
  String email;
  String contactNo;
  String address;
  String fathersName;
  String gender;
  String maritialStatus;
  DateTime dob;
  String bloodGroup;
  DateTime createdAt;
  DateTime updatedAt;

  GetAllStudentsResponse({
    this.id,
    this.name,
    this.email,
    this.contactNo,
    this.address,
    this.fathersName,
    this.gender,
    this.maritialStatus,
    this.dob,
    this.bloodGroup,
    this.createdAt,
    this.updatedAt,
  });

  factory GetAllStudentsResponse.fromJson(Map<String, dynamic> json) => GetAllStudentsResponse(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    contactNo: json["contact_no"],
    address: json["address"],
    fathersName: json["fathers_name"],
    gender: json["gender"],
    maritialStatus: json["maritial_status"],
    dob: DateTime.parse(json["dob"]),
    bloodGroup: json["blood_group"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "contact_no": contactNo,
    "address": address,
    "fathers_name": fathersName,
    "gender": gender,
    "maritial_status": maritialStatus,
    "dob": "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
    "blood_group": bloodGroup,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
