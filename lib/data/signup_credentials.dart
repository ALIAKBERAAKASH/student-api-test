import 'dart:convert';

SignUpCredentials signUpCredentialsFromJson(String str) => SignUpCredentials.fromJson(json.decode(str));

String signUpCredentialsToJson(SignUpCredentials data) => json.encode(data.toJson());

class SignUpCredentials {
  String name;
  String email;
  String password;

  SignUpCredentials({
    this.name,
    this.email,
    this.password,
  });

  factory SignUpCredentials.fromJson(Map<String, dynamic> json) => SignUpCredentials(
    name: json["name"],
    email: json["email"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "email": email,
    "password": password,
  };
}
