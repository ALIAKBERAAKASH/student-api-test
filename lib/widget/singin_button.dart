import 'package:flutter/material.dart';
class LoginButtonPage extends StatelessWidget {
      final String buttonName;
      LoginButtonPage(this.buttonName);
  @override
  Widget build(BuildContext context) {
    var hp =MediaQuery.of(context).size.height;
    var hw =MediaQuery.of(context).size.width;
    return SizedBox(
      width: hw*0.70,
      height: hp*0.05,
      child: RaisedButton(
        child: Text(buttonName,style: TextStyle(color:Colors.white,fontSize: 25,fontStyle: FontStyle.normal,fontWeight:FontWeight.w700),),
        color: Colors.teal,
        onPressed: (){},
        shape: StadiumBorder(),
      ),
    );
  }
}
