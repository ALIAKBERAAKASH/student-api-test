import 'package:flutter/material.dart';

class TextFiledPage extends StatelessWidget {

    final TextEditingController textController;
    final String lebeltext;
    final bool pass;
    TextFiledPage(this.textController,this.lebeltext,this.pass);
      @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: EdgeInsets.all(15),
      child: TextField(
        obscureText:pass ,
        controller: textController ,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: lebeltext,
        ),
      ),
    );
  }
}
