import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:student_api_test/data/get_all_students_response.dart';
import 'package:student_api_test/screens/student_details.dart';

class StudentModel extends StatelessWidget {

  final GetAllStudentsResponse student;

  const StudentModel(this.student);


  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,width: 750, height: 1334, allowFontScaling: true);
    return GestureDetector(
      onTap: () {

        Navigator.of(context).push(MaterialPageRoute(builder: (context) => StudentDetails(student.id)));

      },
      child: Card(
        elevation: 8,
        child: Container(
          margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
          width: double.infinity,
          height:50,
          child:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(student.name,style: TextStyle(color: Colors.black,fontSize: 18,
                      fontStyle: FontStyle.normal),),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(student.contactNo,style: TextStyle(fontSize: 14 ),),
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}
