import 'package:flutter/material.dart';

class ClickSignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Row(
      children: <Widget>[
        Text('Does not have account?'),
        FlatButton(
          textColor: Colors.blue,
          child: Text(
            'Sign in',
            style: TextStyle(fontSize: 20),
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/signup');
          },
        )
      ],
      mainAxisAlignment: MainAxisAlignment.center,
    );
  }
}
